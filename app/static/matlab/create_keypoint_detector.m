function keypoint_detector = create_keypoint_detector(model_fn, matconvnet_dir, gpu_id)
  fprintf(1, '\n\tLoading model : %s', model_fn);
  fprintf(1, '\n\tUsing gpu-id : %d', gpu_id);
  
  keypoint_detector = KeyPointDetector(model_fn, matconvnet_dir, gpu_id);
  fprintf(1, '\n\tReady to process requests from users');
  fprintf(1, '\n');
end
