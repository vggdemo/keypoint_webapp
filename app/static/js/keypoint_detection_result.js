/*
 * Display result of keypoint detection using HTML5 canvas
 *
 * Author: Abhishek Dutta <adutta@robots.ox.ac.uk>
 * Aug. 10, 2016
 */

var keypoint_radius = 0;
var keypoint_fill_color = 'red';

// source: http://stackoverflow.com/questions/3437786/get-the-size-of-the-screen-current-web-page-and-browser-window
var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0],
    x = w.innerWidth || e.clientWidth || g.clientWidth,
    y = w.innerHeight|| e.clientHeight|| g.clientHeight;

var canvas_width = x*0.6 // 60% of the screen width is occupied by the canvas
var canvas_height = y*0.6 // matches the aspect ratio of the image
var scale_factor = 0

// source: https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API
var canvas = document.getElementById("result_canvas");
var ctx = canvas.getContext("2d");
var webdemo_keypoint_annotation = document.getElementById('webdemo_keypoint_annotation');

// load keypoint annotation data from json file
// see: http://stackoverflow.com/questions/14484613/load-local-json-file-into-variable
var jsondata = (function() {
        var json = null;
        $.ajax({
            'async': false,
            'global': false,
            'url': jsonsrc,
            'dataType': "json",
            'success': function (data) {json = data;}});
        return json;})();
var keypoint_count = jsondata["keypoint_count"]
var keypoint_x = new Array(keypoint_count);
var keypoint_y = new Array(keypoint_count);
var keypoint_name = new Array(keypoint_count);
for (var i=0; i<keypoint_count; i++) {
    keypoint_x[i] = jsondata["keypoint_x"][i]
    keypoint_y[i] = jsondata["keypoint_y"][i]
    keypoint_name[i] = jsondata["keypoint_name"][i]
}

// keypoint coordinate transformed for scaled image
var tx_keypoint_x = new Array(keypoint_count);
var tx_keypoint_y = new Array(keypoint_count);

$(document).ready( function () {

  drawImageAndKeypoints();
});

// console.log('jsonsrc = ' + jsonsrc);
// console.log('keypoint_x = ' + keypoint_x.length);
// console.log('keypoint_count = ' + keypoint_count);

// event listener for mouse hover over each keypoint
canvas.addEventListener('click', function(e) {
    //console.log('click: ' + e.offsetX + '/' + e.offsetY);
    hit_id = isInsideKeypoint(e.offsetX, e.offsetY);

    if(hit_id>=0) {
	var annotation_str = keypoint_name[hit_id] + ' at (' + keypoint_x[hit_id] + ', ' + keypoint_y[hit_id] + ')';
	document.getElementById("webdemo_annotation_text").innerHTML = annotation_str;
    }
});

canvas.addEventListener('mousemove', function(e) {
    hit_id = isInsideKeypoint(e.offsetX, e.offsetY);
    if(hit_id>=0) {
	document.getElementById("result_canvas").style.cursor = "pointer";

	webdemo_keypoint_annotation.style.left = e.pageX + 'px';
	webdemo_keypoint_annotation.style.top = e.pageY + 'px';
	webdemo_keypoint_annotation.style.display = "block";
	webdemo_keypoint_annotation.innerHTML = keypoint_name[hit_id];
    } else {
	document.getElementById("result_canvas").style.cursor = "default";
	webdemo_keypoint_annotation.style.display = "none";	
    }
});

canvas.addEventListener('mouseout', function(e) {
    // clear everything from the image
    document.getElementById("webdemo_annotation_text").innerHTML = "Click on the image annotation to get details";
});

function drawImageAndKeypoints() {
    var img = new Image();
    img.addEventListener("load", function() {
	if (this.naturalHeight > this.naturalWidth) {
	    scale_factor = canvas_height/this.naturalHeight;
	    canvas_width = this.naturalWidth * scale_factor;

	} else {
	    // adjust to image width
	    scale_factor = canvas_width/this.naturalWidth;
	    canvas_height = this.naturalHeight * scale_factor;
	}

	// update the keypoints according to the scale factor
	for (i=0; i<keypoint_x.length; i++) {
	    tx_keypoint_x[i] = keypoint_x[i] * scale_factor;
	    tx_keypoint_y[i] = keypoint_y[i] * scale_factor;
	}

	canvas.width = canvas_width;
	canvas.height = canvas_height;
	ctx.drawImage(this, 0, 0, canvas_width, canvas_height);

	// update keypoint rectangle dimension to match that of the image
	keypoint_radius = canvas_width*0.015;

	// draw keypoints over the image
	assert( keypoint_x.length == keypoint_y.length,
		"Different length of keypoint x and y arrays");

	for (i=0; i<keypoint_x.length; i++) {
	    drawKeypoint(tx_keypoint_x[i], tx_keypoint_y[i]);
	}
    }, false);
    img.src = imsrc;
}

function isInsideKeypoint(x, y) {
    var dx, dy;
    for (i=0; i<keypoint_x.length; i++) {
	dx = tx_keypoint_x[i] - x;
	dy = tx_keypoint_y[i] - y;
	if (Math.sqrt(dx*dx + dy*dy) <= keypoint_radius) {
	    return i;
	}
    }
    return -1;
}

function drawKeypoint(x, y) {
    ctx.beginPath();
    ctx.arc(x, y, keypoint_radius, 0, 2 * Math.PI, false);
    ctx.fillStyle = "#E2FFC6";
    ctx.fill();

    ctx.lineWidth = keypoint_radius/4;
    ctx.strokeStyle = "#66CC01";
    ctx.stroke();
}

function assert(condition, message) {
    if (!condition) {
	throw message || "Assertion failed";
    }
}
