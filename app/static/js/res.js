var layer1;
var layer2;
var ctx1;
var ctx2;

var MAX_HEIGHT = 400;
var MAX_WIDTH = 533;
var new_size;
var img = new Image();
var highlight_idx = -1;

// store bb coordinates for the current image:
var text_word, quads, chars;
var highlight_idx = -1;
var mouse_on = false;

// read the prediction json file:
// see: http://stackoverflow.com/questions/14484613/load-local-json-file-into-variable
var dinfo = (function() {
        var json = null;
        $.ajax({
            'async': false,
            'global': false,
            'url': jsonsrc,
            'dataType': "json",
            'success': function (data) {json = data;}});
        return json;})();

function inside(point, vs) {
  // ray-casting algorithm based on
  // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
  var x = point[0], y = point[1];
  var inside = false;
  for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
    var xi = vs[i][0], yi = vs[i][1];
    var xj = vs[j][0], yj = vs[j][1];
    var intersect = ((yi > y) != (yj > y))
        && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
    if (intersect) inside = !inside;
  }
  return inside;
}

function highlight_detection(e) {
  var rect = layer1.getBoundingClientRect();
  var mx = e.clientX - rect.left;
  var my = e.clientY - rect.top; 
  // mark all the boxes in which the mouse is currently in:
  var in_idx = -1;
  var n_quads = quads.length;
  var will_highlight = false;
  for (var i=0; i < n_quads; i++) {
    var in_bb = inside([mx,my], quads[i]);
    if (in_bb) {
      in_idx = i;
      break;
    }
  }

  if (in_idx != highlight_idx) {
    draw_words();
    highlight_idx = -1;
    $("#lex-label").html(""); $("#lex-val").html("&nbsp");
    $("#char-label").html(""); $("#char-val").html("&nbsp");
    if (in_idx > -1) {
      highlight_idx = in_idx;
      draw_quad(ctx1, "#FF3333", quads[in_idx]);
      // update the text here:
      $("#lex-label").html("lexicon:"); $("#lex-val").html("&nbsp" + lex_txt[in_idx]);
      $("#char-label").html("unconstrained:"); $("#char-val").html("&nbsp" + char_txt[in_idx]);
    }
  }
}

function init() {
  layer1 = document.getElementById("layer1");
  $("#layer1").mousemove(highlight_detection);
  ctx1 = layer1.getContext("2d");
  layer2 = document.getElementById("layer2");
  ctx2 = layer2.getContext("2d");
  update_graphic();
}

// resizes the image to limit its maximum side:
function resize_im(in_size) {
  var ms = Math.min(in_size[0],in_size[1]);
  var Ms = Math.max(in_size[0],in_size[1]);
  var out_size = [];
  if (in_size[0] == Ms) {
    out_size = [MAX_WIDTH, Math.ceil(ms*MAX_WIDTH/Ms)];
  } else {
    out_size = [Math.ceil(ms*MAX_HEIGHT/Ms), MAX_HEIGHT];
  }
  //f_scale = [out_size[0]/in_size[0], out_size[1]/in_size[1]];
  return out_size;
}

function resize_canvas() {
  ctx1.clearRect(0, 0, ctx2.canvas.width, ctx2.canvas.height);
  ctx2.clearRect(0, 0, ctx1.canvas.width, ctx1.canvas.height);
  // get the new image size:
  new_size = resize_im([img.width, img.height]);
  // resize the image canvas:
  $("#canvasesdiv").width(new_size[0]).height(new_size[1]);
  ctx2.canvas.width = new_size[0];
  ctx2.canvas.height = new_size[1];
  ctx1.canvas.width = new_size[0];
  ctx1.canvas.height = new_size[1];
}

function update_graphic() {
  highlight_idx = -1;
  // get the image path and draw when loaded:
  img.src = imsrc;
  img.onload = draw_image;
}

function draw_image() {
  resize_canvas();
  ctx2.drawImage(img, 0, 0, new_size[0], new_size[1]);
  get_quads();
  draw_words();
}

function draw_quad(ctx, color, quad) {
  ctx.strokeStyle = color;
  ctx.lineWidth = 2;
  // draw:
  nv = quad.length;
  ctx.beginPath();
  ctx.moveTo(quad[0][0],quad[0][1]);  
  for (var i=1; i < nv; i++) {
    ctx.lineTo(quad[i][0],quad[i][1]);
  }
  ctx.closePath();
  ctx.stroke();
}

function draw_filled_quad(ctx, style, quad) {
  var orig_style = ctx.fillStyle;
  ctx.fillStyle = style;
  // draw:
  nv = quad.length;
  ctx.beginPath();
  ctx.moveTo(quad[0][0],quad[0][1]);  
  for (var i=1; i < nv; i++) {
    ctx.lineTo(quad[i][0],quad[i][1]);
  }
  ctx.closePath();
  ctx.fill();
  ctx.fillStyle = orig_style;
}

function get_quads() {
  var orig_h = dinfo["im_h"];
  var orig_w = dinfo["im_w"];
  var f_h = ctx2.canvas.height / parseFloat(orig_h);
  var f_w = ctx2.canvas.width / parseFloat(orig_w);
  var n_pred = dinfo["n_bb"];  
  lex_txt = Array(n_pred);
  char_txt = Array(n_pred);
  quads = Array(n_pred);
  var text_idx = 0;
  for (var i=0; i < n_pred; i++) {
    char_txt[i] = dinfo["char"][i];
    lex_txt[i] = dinfo["lex"][i];
    var x = f_w * dinfo["wordBB"][i][0];
    var y = f_h * dinfo["wordBB"][i][1];
    var w = f_w * dinfo["wordBB"][i][2];
    var h = f_h * dinfo["wordBB"][i][3];
    quads[i] = [ [x,y], [x+w,y], [x+w,y+h], [x,y+h]];
  }
}

function draw_words() {
  // clear the previous rectangles:
  ctx1.clearRect(0, 0, new_size[0], new_size[1]);
  // word bounding-boxes:
  var n_quads = quads.length;
  for (var i=0; i < n_quads; i++){
    draw_quad(ctx1, "#05DD66", quads[i]);
  }
}

$(document).ready( function () {
  init();
});
