import numpy as np
import os

class KeyPointDetectionBackend(object):

  def __init__(self, app_config):
    #from mlabwrap import mlab
    import mlabwrap

    self.config = app_config;

    if not os.path.isdir(self.config['UPLOAD_FOLDER']):
      os.mkdir( self.config['UPLOAD_FOLDER'] )
    if not os.path.isdir(self.config['JSON_DIR']):
      os.mkdir( self.config['JSON_DIR'] )
    if not os.path.isdir(self.config['CSV_DIR']):
      os.mkdir( self.config['CSV_DIR'] )

    # cache the object:
    self.mlab = mlabwrap.init( self.config['MATLAB_EXEC'])
    
    # add the MATLAB code path:
    self.mlab.addpath(self.config['MATLAB_CODEBASE_DIR'])
    self.mlab.addpath(self.config['MATLAB_CODEBASE_DIR'] + '/lib/jsonlab')

    self.model_fn = self.config['MODEL_BASE_DIR'] + self.config['MODEL_NAME'] + '-v' + self.config['MODEL_VERSION'] + '.mat'
    self.gpu_id = int(os.getenv('GPU_ID', '1'))

    self.KPD = self.mlab.create_keypoint_detector(self.model_fn, self.config['MATCONVNET_DIR'], self.gpu_id);

  def process(self, im_fname):
    """
    loads the image stored at im_fname,
    calls the Matlab text-spotting code on it,
    and returns the resulting JSON.
    """
    return self.mlab.detect_keypoint(self.KPD, im_fname)


    
