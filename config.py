
MODEL_NAME = 'keypoint'
MODEL_VERSION = '2';

MATLAB_EXEC='/usr/local/MATLAB/R2016a/bin/matlab'

'''
## config for docker image
'''
MATLAB_CODEBASE_DIR = '/opt/vgg/vggdemo/keypoint/code/keypoint_webapp/app/static/matlab'
MODEL_BASE_DIR = '/data/vgg/vggdemo/keypoint/data/'
MATCONVNET_DIR = '/opt/vgg/vggdemo/keypoint/code/keypoint_webapp/app/static/matlab/lib/matconvnet-custom/'
OUTPUT_BASEDIR = '/data/vgg/vggdemo/keypoint/appdata/'

# ensure that the environment variable GPU_ID is set and corresponds to 
# the GPU that you want to use for this demo

'''
## config for vggdebug2
MATLAB_CODEBASE_DIR = '/data/adutta/vggdemo/keypoint_webapp/app/static/matlab'
MODEL_BASE_DIR = '/data/adutta/demo-data/keypoint/models/'
MATCONVNET_DIR = '/data/adutta/vggdemo/keypoint_webapp/app/static/matlab/lib/matconvnet-custom/'
OUTPUT_BASEDIR = '/data/adutta/vggdemo_tmp_data/keypoint_webapp/uploads/'
'''

# limit the maximum file size:
MAX_CONTENT_LENGTH = 2 * 1024 * 1024 # 10 MB
# path where the incoming images will be stored:
UPLOAD_FOLDER = OUTPUT_BASEDIR + 'im/'

JSON_DIR = OUTPUT_BASEDIR + 'json/'
CSV_DIR  = OUTPUT_BASEDIR + 'csv/'


